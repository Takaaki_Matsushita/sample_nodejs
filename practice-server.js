const http = require('http');
const fs = require('fs');
var qs = require('querystring');

var server = http.createServer((request,response)=>{
	fs.readFile('./index.html', 'UTF-8',
	(error,data)=>{
		response.writeHead(200, {'Content-Type': 'text/html'});
		response.write(data);
		response.end();
	});

  if (request.method == 'POST') {
    var body = '';
    request.on('data', function (data) {
        body += data;
    });

    request.on('end', function () {
        var post = qs.parse(body);
        console.log(post.name);  //nameの値を取得
    });
  }
});

server.listen(8081);
console.log('サーバ起動中・・・');
